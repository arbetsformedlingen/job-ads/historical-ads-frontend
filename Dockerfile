FROM node:18.20.2-alpine3.19 AS build

WORKDIR /usr/src/app
COPY . .
RUN npm install &&\
    npm run build

FROM nginxinc/nginx-unprivileged:1.25.4-alpine3.18-slim

RUN rm /etc/nginx/conf.d/default.conf
COPY --from=build /usr/src/app/dist/historical-ads-frontend /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 8080

CMD ["nginx", "-g", "daemon off;"]
