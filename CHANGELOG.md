# 1.1.4 (November 2024)
* Removed: JobSearch search function
* Added: Number of open positions displayed in search result view
* Changed: Job advertisement page now opens in new tab
* Fixed: Handling of duplicate advertisements in API response

# 1.0.0
* Initial release
