import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormControl, UntypedFormControl} from "@angular/forms";
import {SearchAdsQuery} from "../model/search-queries.model";
import {SearchAdsService} from "../services/search-ads.service";
import {SearchedAd, SearchedAds} from "../model/searched-ad.model";
import {PageEvent} from "@angular/material/paginator";
import {debounceTime, distinctUntilChanged, filter, map, Observable, startWith, switchMap} from "rxjs";
import {LocationService} from "../services/location.service";
import {faInfoCircle, faLocationDot} from '@fortawesome/free-solid-svg-icons';
import {MatRadioChange} from "@angular/material/radio";
import {Typeahead} from "../model/typeahead.model";
import {TypeaheadService} from "../services/typeahead.service";

@Component({
  selector: 'app-historical-ads-search',
  templateUrl: './historical-ads-search.component.html',
  styleUrls: ['./historical-ads-search.component.scss']
})
export class HistoricalAdsSearchComponent implements OnInit {
  freeTextSearchCtrl: UntypedFormControl;
  fromDateCtrl: UntypedFormControl;
  toDateCtrl: UntypedFormControl;
  employerCtrl: UntypedFormControl;
  employerTypeCtrl: UntypedFormControl;
  searchTypeCtrl: UntypedFormControl;
  locationsCtrl: FormControl;
  sortByCtrl: UntypedFormControl;
  defaultPageSize = 50;
  historicalMaxPageSize = 100;
  pageSize = this.defaultPageSize;
  pageNumber = 0;
  totalSize = 0;
  totalVacancies = 0;
  historicalMaxSizeReached = false;
  historicalMaxSixe = 2000;
  filteredLocations: Observable<any[]> | undefined;
  freeTextSearchTypeahead: Observable<any[]> | undefined;
  faLocationDot = faLocationDot;
  faInfoCircle = faInfoCircle;
  searchAdsResult: SearchedAds;


  constructor(
    public searchAdsService: SearchAdsService,
    private locationService: LocationService,
    private typeAheadService: TypeaheadService,
  ) {
    this.freeTextSearchCtrl = new UntypedFormControl();
    this.fromDateCtrl = new UntypedFormControl();
    this.toDateCtrl = new UntypedFormControl();
    this.employerCtrl = new UntypedFormControl();
    this.searchTypeCtrl = new UntypedFormControl();
    this.employerTypeCtrl = new UntypedFormControl();
    this.employerTypeCtrl.setValue("orgnr")
    this.locationsCtrl = new FormControl<string>('', [
      CustomValidators.forbiddenObjectValidator
    ]);
    this.sortByCtrl = new UntypedFormControl();
    this.totalSize = 0;
    this.totalVacancies = 0;
    this.searchAdsResult = new SearchedAds();
  }

  sortByOptions = [
    { value: 'relevance', viewValue: 'Relevans' },
    { value: 'pubdate-desc', viewValue: 'Publiceringsdatum (desc)' },
    { value: 'pubdate-asc', viewValue: 'Publiceringsdatum (asc)' },
    { value: 'applydate-desc', viewValue: 'Ansökningsdatum (desc)' },
    { value: 'applydate-asc', viewValue: 'Ansökningsdatum (asc)' },
    { value: 'updated', viewValue: 'Senast uppdaterad' },
  ];

  ngOnInit(): void {
    this.filteredLocations = this.locationsCtrl.valueChanges.pipe(
      startWith(''),
      map(location =>
        location ? this.filterLocations(location) : this.locationService.getLocations().slice()
      )
    );
    this.freeTextSearchTypeahead = this.freeTextSearchCtrl.valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(500),
      filter((freeText) => !!freeText),
      switchMap(freeText => this.typeAheadService.fetchTypeahead(HistoricalAdsSearchComponent.getValueFromFreeTextSearchCtrl(freeText)))
    );
  }

  public paginatedSearch(e: PageEvent): PageEvent {
    const pageNumber = e.pageIndex;
    const pageSize = e.pageSize;
    this.search(pageNumber, pageSize);
    return e;
  }

  filterLocations(value: any) {
    const searchstr = typeof value === 'string' ? value.toLowerCase() : value.key.toLowerCase();
    // @ts-ignore
    return this.locationService.getLocations().filter(({value: location}) => {
      if (location) {
        return location.toLowerCase().includes(searchstr);
      }
      }
    ).sort((a: any, b: any) => this.sortTypeaheadList(a, b, searchstr));
  }

  createSearchQuery(): SearchAdsQuery {
    let searchAdsQuery: SearchAdsQuery = new SearchAdsQuery();
    let fromDate: Date = new Date();
    fromDate = this.fromDateCtrl.value;
    let toDate: Date = new Date();
    toDate = this.toDateCtrl.value;
    if (fromDate) {
      searchAdsQuery.historicalFrom = fromDate.toDateString();
    }
    if (toDate) {
      searchAdsQuery.historicalTo = toDate.toDateString();
    }
    if (this.employerCtrl.value) {
      if (this.employerTypeCtrl.value == "orgnr") {
        searchAdsQuery.employer = this.employerCtrl.value.replace(/\D/g,'');
      }
      else if (this.employerTypeCtrl.value == "name") {
        searchAdsQuery.employer = this.employerCtrl.value;
      }
    }
    if (this.locationsCtrl.value) {
      searchAdsQuery.municipality = this.locationsCtrl.value.key;
    }
    if(this.sortByCtrl.value){
      searchAdsQuery.sort = this.sortByCtrl.value;
    }
    searchAdsQuery.q = HistoricalAdsSearchComponent.getValueFromFreeTextSearchCtrl(this.freeTextSearchCtrl.value)
    return searchAdsQuery

  }

  search(pageNumber: number | undefined, pageSize: number | undefined): void {
    let searchAdsQuery: SearchAdsQuery = this.createSearchQuery();
    if (!pageSize) {
      pageSize = 0;
    }
    let offset = (pageSize * this.pageNumber);
    if (offset + pageSize > this.historicalMaxSixe) {
      offset = this.historicalMaxSixe - pageSize;
    }
    const limit = pageSize;
    searchAdsQuery.offset = offset
    searchAdsQuery.limit = limit
    this.searchAdsService.searchAds(searchAdsQuery, pageNumber, pageSize);
  }

  _getSearchedAds(): SearchedAd[] {
    this.searchAdsService.searchedAdsResult.subscribe(searchres => {
      {
        this.searchAdsResult = searchres;
        if (searchres.pageSize) {
          this.pageSize = searchres.pageSize;
        }
        else {
          this.pageSize = this.defaultPageSize;
        }

        if (searchres.pageNumber) {
          this.pageNumber = searchres.pageNumber;
        }
        else {
          this.pageNumber = 0;
        }
      }
    });
    return this.searchAdsResult.ads;
  }

  selectLocation(event: any) {
    console.log('selected location:' + JSON.stringify(event.option.value));
    this.search(0, this.defaultPageSize);
  }

  displayFnLocation(location: any): string {
    return location && location.value ? location.value : '';
  }

  displayFnFreeText(freeText: Typeahead | undefined): string {
    return freeText && freeText.value ? freeText.value : '';
  }

  numberOfHitsText(): string {
    if (!this.searchAdsResult.totalHits) {
      this.totalSize = 0;
      this.totalVacancies = 0;
      this.historicalMaxSizeReached = false;
      return "Inga sökträffar";
    }
    
    this.totalSize = this.searchAdsResult.totalHits;
    this.historicalMaxSizeReached = false;
    
    if (!this.searchAdsResult.totalPositions) {
      return "Antal sökträffar: " + this.totalSize;
    }
    
    this.totalVacancies = this.searchAdsResult.totalPositions;
    return this.totalSize + " annonser med " + this.totalVacancies + " jobb";
  }

  private static getValueFromFreeTextSearchCtrl(value: any): string {
    if (value && value.value) {
      // User has chosen an item from the typeahead list (type Object)
      return value.value;
    }
    // User has not chosen an item from the typeahead list (type String)
    return value;
  }

  sortTypeaheadList(a: any, b: any, searchstr: any) {
    const aLower = a.key.toLowerCase();
    const bLower = b.key.toLowerCase();
    const searchstrLower = searchstr.toLowerCase();

    if (aLower.startsWith(searchstrLower) && !(bLower.startsWith(searchstrLower))) {
      return -1;
    }

    if (bLower.startsWith(searchstrLower) && !(aLower.startsWith(searchstrLower))) {
      return 1;
    }

    return 0;
  }

  fromDateChanged(event: any) {
    if (event) {
      this.search(0, this.defaultPageSize);
    }
  }

  toDateChanged(event: any) {
     if (event) {
       this.search(0, this.defaultPageSize);
     }
  }

  employerTypeChanged($event: MatRadioChange) {
    if ($event.value === 'orgnr' || $event.value === 'name') {
      this.search(0, this.defaultPageSize);
    }
  }

  sortByChanged(event: any) {
    if (event) {
      this.search(0, this.defaultPageSize);
    }
 }

  saveAdsToFile(){
    const adsToSave: SearchedAd[] = this._getSearchedAds()
    var content: string = "";
    const fileName: string = "annonser_sid_" + (this.pageNumber + 1) + "_" + new Date().toLocaleString() + ".txt";
    
    for (let adToSave of adsToSave) {
      content += "Annons id: " + adToSave.adId + "\nTitel: " + adToSave.headline + "\n";
      if (adToSave.employer && adToSave.employer.name) {
        content += "Arbetsgivare: " + adToSave.employer.name + "\n";
      }
      if (adToSave.occupation && adToSave.occupation.label) {
        content += "Yrke: " + adToSave.occupation.label + "\n";
      }
       content += "Publiceringsperiod: " + adToSave.publicationDate + " - " + adToSave.lastPublicationDate + "\n";
      if (adToSave.workingHoursType && adToSave.workingHoursType.label) {
        content += "Omfattning: " + adToSave.workingHoursType.label + "\n";
      }
      if (adToSave.duration && adToSave.duration.label) {
        content += "Varaktighet: " + adToSave.duration.label + "\n";
      }
      if (adToSave.employmentType && adToSave.employmentType.label) {
        content += "Anställningsform: " + adToSave.employmentType.label + "\n";
      }
      if (adToSave.numberOfVacancies) {
        content += "Antal tjänster: " + adToSave.numberOfVacancies + "\n";
      }
      content += "Kvalifikationer:\n";
      if (adToSave.experienceRequired) {
        content += "Arbetserfarenhet krävs\n";
      }
      if (adToSave.accessToOwnCar) {
        content += "Tillgång till egen bil\n";
      }
      if (adToSave.drivingLicenseRequired) {
        content += "Körkort krävs\n";
      }
      content += "Arbetslivserfarenhet:\n";
      if (adToSave.niceToHave && adToSave.niceToHave.workExperiences && adToSave.niceToHave.workExperiences.length > 0) {
        adToSave.niceToHave.workExperiences.forEach((niceToHaveExperience) => content += niceToHaveExperience + "(meriterande)\n")
      }
      if (adToSave.mustHave && adToSave.mustHave.workExperiences && adToSave.mustHave.workExperiences.length > 0) {
        adToSave.mustHave.workExperiences.forEach((mustHaveExperience) => content += mustHaveExperience + "(krav)\n")
      }
      content += "Utbildning:\n";
      if (adToSave.niceToHave && adToSave.niceToHave.education && adToSave.niceToHave.education.length > 0) {
        adToSave.niceToHave.education.forEach((niceToHaveEducation) => content += niceToHaveEducation + "(meriterande)\n")
      }
      if (adToSave.mustHave && adToSave.mustHave.education && adToSave.mustHave.education.length > 0) {
        adToSave.mustHave.education.forEach((mustHaveEducation) => content += mustHaveEducation + "(krav)\n")
      }
      content += "Kompetenser:\n";
      if (adToSave.niceToHave && adToSave.niceToHave.skills && adToSave.niceToHave.skills.length > 0) {
        adToSave.niceToHave.skills.forEach((niceToHaveSkill) => content += niceToHaveSkill + "(meriterande)\n")
      }
      if (adToSave.mustHave && adToSave.mustHave.skills && adToSave.mustHave.skills.length > 0) {
        adToSave.mustHave.skills.forEach((mustHaveSkill) => content += mustHaveSkill + "(krav)\n")
      }
      content += "Språk:\n";
      if (adToSave.niceToHave && adToSave.niceToHave.languages && adToSave.niceToHave.languages.length > 0) {
        adToSave.niceToHave.languages.forEach((niceToHaveLanguage) => content += niceToHaveLanguage + "(meriterande)\n")
      }
      if (adToSave.mustHave && adToSave.mustHave.languages && adToSave.mustHave.languages.length > 0) {
        adToSave.mustHave.languages.forEach((mustHaveLanguage) => content += mustHaveLanguage + "(krav)\n")
      }
       if (adToSave.description && adToSave.description.text) {
         content += "Om jobbet:\n" + adToSave.description.text + "\n";
       }
       if (adToSave.description && adToSave.description.companyInformation) {
        content += "Om arbetsgivaren:\n" + adToSave.description.companyInformation + "\n";
      }
      content += "Om anställningen:\n";
      if (adToSave.salaryDescription) {
        content += adToSave.salaryDescription + "\n";
      }
      if (adToSave.salaryType) {
        content += "Lönetyp: " + adToSave.salaryType.label + "\n";
      }
      content += "Arbetsplatsens adress:\n";
      if (adToSave.workplaceAddress && adToSave.workplaceAddress.streetAddress) {
        content += adToSave.workplaceAddress.streetAddress + "\n";
      }
      if (adToSave.workplaceAddress && adToSave.workplaceAddress.postcode) {
        content += adToSave.workplaceAddress.postcode + " ";
      }
      if (adToSave.workplaceAddress && adToSave.workplaceAddress.city) {
        content += adToSave.workplaceAddress.city + "\n";
      }
      if (adToSave.workplaceAddress && adToSave.workplaceAddress.municipality) {
        content += "Kommun: " + adToSave.workplaceAddress.municipality + "\n";
      }
      content += "Arbetsgivaren:\n";
      if (adToSave.employer && adToSave.employer.name) {
        content += adToSave.employer.name + "\n";
      }
      if (adToSave.employer && adToSave.employer.url) {
        content += adToSave.employer.url + "\n";
      }
      if (adToSave.id) {
        content += "Id: " + adToSave.id + "\n";
      }
      if (adToSave.sourceType && adToSave.sourceType != 'VIA_ANNONSERA' && adToSave.sourceType != 'VIA_PLATSBANKEN_DXA') {
        content += "Annonskälla: " + adToSave.sourceType + "\n";
      }
       content += "\n";
    }
    this.writeContents(content, fileName, 'text/plain');
  }

  private writeContents(content: string, fileName: string, contentType: string) {
    var a = document.createElement('a');
    var file = new Blob([content], {type: contentType});
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
  }
}

export class CustomValidators {

  static forbiddenObjectValidator(control: AbstractControl): any {
    // Validate if value is an object (and not e.g. a string value):
    if (control.value) {
      return typeof control.value !== 'object' || control.value === null ? { 'forbiddenObject': true } : null;
    }
    return;
  }
}
