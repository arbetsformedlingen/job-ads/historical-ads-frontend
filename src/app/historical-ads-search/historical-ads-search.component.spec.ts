import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalAdsSearchComponent } from './historical-ads-search.component';

describe('HistoricalAdsSearchComponent', () => {
  let component: HistoricalAdsSearchComponent;
  let fixture: ComponentFixture<HistoricalAdsSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoricalAdsSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricalAdsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
