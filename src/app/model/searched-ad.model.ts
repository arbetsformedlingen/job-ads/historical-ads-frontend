export class SearchedAds {
  ads: SearchedAd[] = new Array();
  totalHits: number = 0;
  totalPositions: number = 0;
  // offset: number | undefined;
  // limit: number | undefined;
  pageSize: number | undefined;
  pageNumber: number | undefined;

}
export class SearchedAd {
  id: string | undefined;
  adId: string | undefined;
  externalId: string | undefined;
  webpageUrl: string | undefined;
  logoUrl: string | undefined;
  headline: string | undefined;
  applicationDeadline: string | undefined | null;
  numberOfVacancies: string | undefined;
  description: Description | undefined;
  employmentType: EmploymentType | undefined;
  salaryType: SalaryType | undefined;
  duration: Duration | undefined;
  workingHoursType: WorkingHoursType | undefined;
  scopeOfWork: ScopeOfWork | undefined;
  employer: Employer | undefined;
  applicationDetails: ApplicationDetails | undefined;
  salaryDescription: string | undefined;
  access: string | undefined;
  experienceRequired: boolean | undefined;
  accessToOwnCar: boolean | undefined;
  drivingLicenseRequired: boolean | undefined;
  drivingLicense: DrivingLicence | undefined;
  occupation: Occupation | undefined;
  occupationGroup: OccupationGroup | undefined;
  occupationField: OccupationField | undefined;
  workplaceAddress: WorkplaceAddress | undefined;
  mustHave: MustHave | undefined;
  niceToHave: NiceToHave | undefined;
  applicationContacts: string[] = new Array();
  publicationDate: string | undefined | null;
  lastPublicationDate: string | undefined | null;
  removed: string | undefined; // TODO bool?
  removedDate: string | undefined;
  sourceType: string | undefined;
  timestamp: string | undefined;
}

export class Description {
  text: string | undefined;
  textFormatted: string | undefined;
  companyInformation: string | undefined;
  needs: string | undefined;
  requirements: string | undefined;
  conditions: string | undefined;
}

export class EmploymentType {
  conceptId: string | undefined;
  label: string | undefined;
  legacyAmsTaxonomyId: string | undefined;
  localizedLabel: string | undefined;
}

export class SalaryType {
  conceptId: string | undefined;
  label: string | undefined;
  legacyAmsTaxonomyId: string | undefined;
}

export class Duration {
  conceptId: string | undefined;
  label: string | undefined;
  legacyAmsTaxonomyId: string | undefined;
}

export class WorkingHoursType {
  conceptId: string | undefined;
  label: string | undefined;
  legacyAmsTaxonomyId: string | undefined;
}

export class ScopeOfWork {
  min: string | undefined;
  max: string | undefined;
}

export class Employer {
  phoneNumber: string | undefined;
  email: string | undefined;
  url: string | undefined;
  organizationNumber: string | undefined;
  name: string | undefined;
  workplace: string | undefined;
}

export class ApplicationDetails {
  information: string | undefined;
  reference: string | undefined;
  email: string | undefined;
  viaAf: string | undefined;
  url: string | undefined;
  other: string | undefined;
}

export class DrivingLicence {
  conceptId: string | undefined;
  label: string | undefined;
  legacyAmsTaxonomyId: string | undefined;
}

export class Occupation {
  conceptId: string | undefined;
  label: string | undefined;
  legacyAmsTaxonomyId: string | undefined;
}

export class OccupationGroup {
  conceptId: string | undefined;
  label: string | undefined;
  legacyAmsTaxonomyId: string | undefined;
}

export class OccupationField {
  conceptId: string | undefined;
  label: string | undefined;
  legacyAmsTaxonomyId: string | undefined;
}

export class WorkplaceAddress {
  municipality: string | undefined;
  municipalityCode: string | undefined;
  municipalityConceptId: string | undefined;
  region: string | undefined;
  regionCode: string | undefined;
  regionConceptId: string | undefined;
  country: string | undefined;
  countryCode: string | undefined;
  countryConceptId: string | undefined;
  streetAddress: string | undefined;
  postcode: string | undefined;
  city: string | undefined;
  coordinates: string[] = new Array();
}

export class MustHave {
  skills: string[] = new Array();
  languages: string[] = new Array();
  workExperiences: string[] = new Array();
  education: string[] = new Array();
  educationLevel: string[] = new Array();
}

export class NiceToHave {
  skills: string[] = new Array();
  languages: string[] = new Array();
  workExperiences: string[] = new Array();
  education: string[] = new Array();
  educationLevel: string[] = new Array();
}


