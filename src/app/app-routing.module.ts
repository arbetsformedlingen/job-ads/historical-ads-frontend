import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HistoricalAdDetailsComponent } from "./historical-ad-details/historical-ad-details.component";
import { HistoricalAdsSearchComponent } from "./historical-ads-search/historical-ads-search.component";

const routes: Routes = [
  { path: '', redirectTo: '/annonser', pathMatch: 'full' },
  { path: 'annonser', component: HistoricalAdsSearchComponent },
  { path: 'annonser/historisk/:id', component: HistoricalAdDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
