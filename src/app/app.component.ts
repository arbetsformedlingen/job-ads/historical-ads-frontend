import { Component } from '@angular/core';
import {environment} from "../environments/environment";
import {version} from "../environments/version";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'historical-ads-frontend';
  historicalAdsUrl = environment.settings.historicalAdsApi;
  jobSearchUrl = environment.settings.jobSearchApi;
  frontendVersion = version.frontendVersion;
}
