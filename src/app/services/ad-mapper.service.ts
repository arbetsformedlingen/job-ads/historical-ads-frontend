import {Injectable} from "@angular/core";
import {
  ApplicationDetails,
  Description, DrivingLicence,
  Duration, Employer,
  EmploymentType, MustHave, NiceToHave, Occupation, OccupationField, OccupationGroup,
  SalaryType, ScopeOfWork,
  SearchedAd,
  WorkingHoursType, WorkplaceAddress
} from "../model/searched-ad.model";
import {DatePipe} from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class AdMapperService {

  constructor(public datepipe: DatePipe) {
  }
  /* Map from an adIn (i.e. json from backend REST response to an internal SearchedAd) */
  public adInToSearchedAdMapper(adIn: any, searchType: string | undefined): SearchedAd {
    let searchedAd: SearchedAd = new SearchedAd();
    searchedAd.id = adIn['id'];
    if (searchType == 'current') {
      searchedAd.adId = adIn['id'];
    } else {
      searchedAd.adId = adIn['original_id'];
    }
    searchedAd.sourceType = adIn['source_type'];
    searchedAd.headline = adIn['headline'];
    searchedAd.logoUrl = adIn['logo_url'];
    searchedAd.externalId = adIn['external_id'];
    searchedAd.numberOfVacancies = adIn['number_of_vacancies']
    searchedAd.webpageUrl = adIn['webpage_url'];
    if (adIn['application_deadline']) {
      searchedAd.applicationDeadline = this.datepipe.transform(adIn['application_deadline'], 'yyyy-MM-dd');
    }
    if (adIn['last_publication_date']) {
      searchedAd.lastPublicationDate = this.datepipe.transform(adIn['last_publication_date'], 'yyyy-MM-dd');
    }
    if (adIn['publication_date']) {
      searchedAd.publicationDate = this.datepipe.transform(adIn['publication_date'], 'yyyy-MM-dd');
    }
    const descriptionIn = adIn['description'];
    if (descriptionIn) {
      let description: Description = new Description();
      description.text = descriptionIn['text'];
      description.textFormatted = descriptionIn['text_formatted'];
      description.companyInformation = descriptionIn['company_information'];
      description.needs = descriptionIn['needs'];
      description.requirements = descriptionIn['requirements']
      description.conditions = descriptionIn['conditions'];
      searchedAd.description = description;
    }
    const employmentTypeIn = adIn['employment_type'];
    if (employmentTypeIn) {
      let employmentType: EmploymentType = new EmploymentType();
      employmentType.conceptId = employmentTypeIn['concept_id']
      employmentType.label = employmentTypeIn['label']
      employmentType.legacyAmsTaxonomyId = employmentTypeIn['legacy_ams_taxonomy_id'];
      searchedAd.employmentType = employmentType;
    }

    const salaryTypeIn = adIn['salary_type'];
    if (salaryTypeIn) {
      let salaryType: SalaryType = new SalaryType();
      salaryType.conceptId = salaryTypeIn['concept_id'];
      salaryType.label = salaryTypeIn['label'];
      salaryType.legacyAmsTaxonomyId = salaryTypeIn['legacy_ams_taxonomy_id'];
      searchedAd.salaryType = salaryType;
    }

    searchedAd.salaryDescription = salaryTypeIn['salary_description'];

    const durationIn = adIn['duration'];
    if (durationIn) {
      let duration: Duration = new Duration();
      duration.conceptId = durationIn['concept_id']
      duration.label = durationIn['label'];
      duration.legacyAmsTaxonomyId = durationIn['legacy_ams_taxonomy_id'];
      searchedAd.duration = duration;
    }

    const workingHoursTypeIn = adIn['working_hours_type'];
    if (workingHoursTypeIn) {
      let workingHoursType: WorkingHoursType = new WorkingHoursType();
      workingHoursType.conceptId = workingHoursTypeIn['concept_id']
      workingHoursType.label = workingHoursTypeIn['label'];
      workingHoursType.legacyAmsTaxonomyId = workingHoursTypeIn['legacy_ams_taxonomy_id'];
      searchedAd.workingHoursType = workingHoursType;
    }

    const scopeOfWorkIn = adIn['scope_of_work'];
    if (scopeOfWorkIn) {
      let scopeOfWork: ScopeOfWork = new ScopeOfWork();
      scopeOfWork.max = scopeOfWorkIn['max'];
      scopeOfWork.min = scopeOfWorkIn['min'];
      searchedAd.scopeOfWork = scopeOfWork;
    }

    searchedAd.access = adIn['access'];

    const employerIn = adIn['employer'];
    if (employerIn) {
      let employer: Employer = new Employer();
      employer.url = employerIn['url'];
      employer.name = employerIn['name'];
      employer.email = employerIn['email'];
      employer.organizationNumber = employerIn['organization_number'];
      employer.phoneNumber = employerIn['phone_number'];
      employer.workplace = employerIn['workplace'];
      searchedAd.employer = employer;
    }

    const applicationDetailIn = adIn['application_details'];
    if (applicationDetailIn) {
      let applicationDetails: ApplicationDetails = new ApplicationDetails();
      applicationDetails.url = applicationDetailIn['url'];
      applicationDetails.email = applicationDetailIn['email'];
      applicationDetails.information = applicationDetailIn['information'];
      applicationDetails.other = applicationDetailIn['other'];
      applicationDetails.viaAf = applicationDetailIn['via_af'];
      applicationDetails.reference = applicationDetailIn['reference'];
      searchedAd.applicationDetails = applicationDetails;
    }

    searchedAd.experienceRequired = adIn['experience_required'];
    searchedAd.accessToOwnCar = adIn['access_to_own_car'];
    searchedAd.drivingLicenseRequired = adIn['driving_license_required'];
    const drivingLicenceIn = adIn['driving_license'];
    if (drivingLicenceIn) {
      let drivingLicence: DrivingLicence = new DrivingLicence();
      drivingLicence.conceptId = drivingLicenceIn['concept_id'];
      drivingLicence.label = drivingLicenceIn['label'];
      drivingLicence.legacyAmsTaxonomyId = drivingLicenceIn['legacy_ams_taxonomy_id'];
      searchedAd.drivingLicense = drivingLicence;
    }

    const occupationIn = adIn['occupation'];
    if (occupationIn) {
      let occupation: Occupation = new Occupation();
      occupation.conceptId = occupationIn['concept_id'];
      occupation.label = occupationIn['label'];
      occupation.legacyAmsTaxonomyId = occupationIn['legacy_ams_taxonomy_id'];
      searchedAd.occupation = occupation
    }
    const occupationGroupIn = adIn['occupation_group'];
    if (occupationGroupIn) {
      let occupationGroup: OccupationGroup = new OccupationGroup();
      occupationGroup.conceptId = occupationGroupIn['concept_id'];
      occupationGroup.label = occupationGroupIn['label'];
      occupationGroup.legacyAmsTaxonomyId = occupationGroupIn['legacy_ams_taxonomy_id'];
      searchedAd.occupationGroup = occupationGroup;
    }
    const occupationFieldIn = adIn['occupation_field'];
    if (occupationFieldIn) {
      let occupationField: OccupationField = new OccupationField();
      occupationField.conceptId = occupationFieldIn['concept_id'];
      occupationField.label = occupationFieldIn['label'];
      occupationField.legacyAmsTaxonomyId = occupationFieldIn['legacy_ams_taxonomy_id'];
      searchedAd.occupationField = occupationField;
    }

    const workplaceAddressIn = adIn['workplace_address'];
    if (workplaceAddressIn) {
      let workPlaceAddress: WorkplaceAddress = new WorkplaceAddress();
      workPlaceAddress.municipality = workplaceAddressIn['municipality'];
      workPlaceAddress.municipalityCode = workplaceAddressIn['municipality_code'];
      workPlaceAddress.municipalityConceptId = workplaceAddressIn['municipality_concept_id'];
      workPlaceAddress.region = workplaceAddressIn['region'];
      workPlaceAddress.regionCode = workplaceAddressIn['region_code'];
      workPlaceAddress.regionConceptId = workplaceAddressIn['region_concept_id'];
      workPlaceAddress.country = workplaceAddressIn['country'];
      workPlaceAddress.countryCode = workplaceAddressIn['country_code'];
      workPlaceAddress.countryConceptId = workplaceAddressIn['country_concept_id'];
      workPlaceAddress.streetAddress = workplaceAddressIn['street_address'];
      workPlaceAddress.postcode = workplaceAddressIn['postcode'];
      workPlaceAddress.city = workplaceAddressIn['city'];

      const coordinates = workplaceAddressIn['coordinates'];
      if (coordinates) {
        for (let coordinate of coordinates) {
          workPlaceAddress.coordinates.push(coordinate);
        }
      }
      searchedAd.workplaceAddress = workPlaceAddress;
    }

    const mustHaveIn = adIn['must_have'];
    if (mustHaveIn) {
      let mustHave: MustHave = new MustHave();

      const mustSkills = mustHaveIn['skills'];
      if (mustSkills) {
        for (let mustSkill of mustSkills) {
          mustHave.skills.push(mustSkill['label']);
        }
      }

      const mustLanguages = mustHaveIn['languages'];
      if (mustLanguages) {
        for (let mustLanguage of mustLanguages) {
          mustHave.languages.push(mustLanguage['label']);
        }
      }

      const mustWorkExperiences = mustHaveIn['work_experiences'];
      if (mustWorkExperiences) {
        for (let mustWorkExperience of mustWorkExperiences) {
          mustHave.workExperiences.push(mustWorkExperience['label']);
        }
      }

      const mustEducations = mustHaveIn['education'];
      if (mustEducations) {
        for (let mustEducation of mustEducations) {
          mustHave.education.push(mustEducation['label']);
        }
      }

      const mustEducationLevels = mustHaveIn['education_level'];
      if (mustEducationLevels) {
        for (let mustEducationLevel of mustEducationLevels) {
          mustHave.educationLevel.push(mustEducationLevel['label']);
        }
      }

      searchedAd.mustHave = mustHave;
    }

    const niceToHaveIn = adIn['nice_to_have'];
    if (niceToHaveIn) {
      let niceToHave: NiceToHave = new NiceToHave();

      const niceToHaveSkills = niceToHaveIn['skills'];
      if (niceToHaveSkills) {
        for (let mustSkill of niceToHaveSkills) {
          if (mustSkill['label']) {
            niceToHave.skills.push(mustSkill['label']);
          }
        }
      }

      const niceToHaveLanguages = niceToHaveIn['languages'];
      if (niceToHaveLanguages) {
        for (let niceToHaveLanguage of niceToHaveLanguages) {
          if (niceToHaveLanguage['label']) {
            niceToHave.languages.push(niceToHaveLanguage['label']);
          }
        }
      }

      const niceToHaveExperiences = niceToHaveIn['work_experiences'];
      if (niceToHaveExperiences) {
        for (let niceToHaveExperience of niceToHaveExperiences) {
          if (niceToHaveExperience['label']) {
            niceToHave.workExperiences.push(niceToHaveExperience['label']);
          }
        }
      }

      const niceToHaveEducations = niceToHaveIn['education'];
      if (niceToHaveEducations) {
        for (let niceToHaveEducation of niceToHaveEducations) {
          if (niceToHaveEducation['label']) {
            niceToHave.education.push(niceToHaveEducation['label']);
          }
        }
      }

      const niceToHaveEducationLevels = niceToHaveIn['education_level'];
      if (niceToHaveEducationLevels) {
        for (let niceToHaveEducationLevel of niceToHaveEducationLevels) {
          if (niceToHaveEducationLevel['label']) {
            niceToHave.educationLevel.push(niceToHaveEducationLevel['label']);
          }
        }
      }

      searchedAd.niceToHave = niceToHave;
    }
    return searchedAd;
  }

}
