import {Injectable} from "@angular/core";
import {SearchAdsQuery} from "../model/search-queries.model";
import {HttpClient} from "@angular/common/http";
import {
  SearchedAd
} from "../model/searched-ad.model";
import {map, Observable, of} from "rxjs";
import {environment} from "../../environments/environment";
import {Typeahead} from "../model/typeahead.model";

@Injectable({
  providedIn: 'root'
})
export class TypeaheadService {

  constructor(private httpClient: HttpClient) {
  }

  private backendUrl = environment.settings.jobSearchApi;

  public fetchTypeahead(query: String | null): Observable<Typeahead[]> {
    const url = `${this.backendUrl}/complete?q=${query}&limit=10&contextual=true`;
    return this.httpClient.get(url)
      .pipe(map(data =>
        this.handleResponse(data)
      ));
  }

  public handleResponse(typeaheadsIn: any): Typeahead[] {
    let typeaheads: Typeahead[] = new Array();
    if (typeaheadsIn) {
      for (let typeaheadIn of typeaheadsIn['typeahead']) {
        let typeahead = new Typeahead();
        typeahead.value = typeaheadIn['value'];
        typeahead.type = typeaheadIn['type'];
        typeahead.foundPhrase = typeaheadIn['found_phrase'];
        typeahead.occurences = typeaheadIn['occurences'];
        typeaheads.push(typeahead);
      }
    }
    return typeaheads;
  }

}
