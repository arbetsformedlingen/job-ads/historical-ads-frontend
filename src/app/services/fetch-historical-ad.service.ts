import {Injectable} from "@angular/core";
import {SearchAdsQuery} from "../model/search-queries.model";
import {HttpClient} from "@angular/common/http";
import {
  ApplicationDetails,
  Description, DrivingLicence,
  Duration, Employer,
  EmploymentType, MustHave, NiceToHave, Occupation, OccupationField, OccupationGroup,
  SalaryType, ScopeOfWork,
  SearchedAd,
  WorkingHoursType, WorkplaceAddress
} from "../model/searched-ad.model";
import {map, Observable, of} from "rxjs";
import {environment} from "../../environments/environment";
import {DatePipe} from "@angular/common";
import {AdMapperService} from "./ad-mapper.service";

@Injectable({
  providedIn: 'root'
})
export class FetchHistoricalAdService {

  constructor(private httpClient: HttpClient, public datepipe: DatePipe,
              private adMapperService: AdMapperService) {
  }

  private _searchedAd: SearchedAd = new SearchedAd();

  public get searchedAdResult(): Observable<SearchedAd> {
    return of(this._searchedAd);
  }

  private backendUrl = environment.settings.historicalAdsApi;

  public fetchHistoricalAd(id: String | null): Observable<SearchedAd> {
      const url = `${this.backendUrl}/ad/${id}`;
      return this.httpClient.get(url)
        .pipe(map(data =>
            this.handleResponse(data)
      ));
  }


  public handleResponse(adIn: any): SearchedAd {
    let searchedAd: SearchedAd = new SearchedAd();
    if (adIn) {
      searchedAd = this.adMapperService.adInToSearchedAdMapper(adIn, "historical");
    }
    return searchedAd;
  }

}
