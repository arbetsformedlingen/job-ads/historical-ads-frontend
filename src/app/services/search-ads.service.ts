import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";
import { SearchAdsQuery } from "../model/search-queries.model";
import { catchError, Observable, of, throwError } from "rxjs";
import {
  SearchedAd,
  SearchedAds,
} from "../model/searched-ad.model";
import { DatePipe } from "@angular/common";
import { AdMapperService } from "./ad-mapper.service";

@Injectable({
  providedIn: 'root'
})
export class SearchAdsService {

  private _searchAdsQuery: SearchAdsQuery | undefined = new SearchAdsQuery();

  public isLoadingPageResult: Boolean = false

  public searchType: string = 'historical';

  constructor(private httpClient: HttpClient, public datepipe: DatePipe,
    private adMapperService: AdMapperService) {
  }

  private _searchedAds: SearchedAds = new SearchedAds();

  public get searchedAdsResult(): Observable<SearchedAds> {
    return of(this._searchedAds);
  }

  private historicalAdsUrl = environment.settings.historicalAdsApi;

  private createBaseSearchAdsURI(url: string, searchAdsQuery: SearchAdsQuery): string {
    if (searchAdsQuery.xFeatureEnableFalseNegative) {
      url += `x-feature-enable-false-negative=${searchAdsQuery.xFeatureEnableFalseNegative}&`
    }
    if (searchAdsQuery.publishedBefore) {
      const publishedBefore = this.datepipe.transform(searchAdsQuery.publishedBefore, 'yyyy-MM-ddThh:mm:ss');
      url += `&published-before=${publishedBefore}&`
    }
    if (searchAdsQuery.publishedAfter) {
      const publishedAfter = this.datepipe.transform(searchAdsQuery.publishedAfter, 'yyyy-MM-ddThh:mm:ss');
      url += `published-after=${publishedAfter}&`
    }
    if (searchAdsQuery.occupationName) {
      url += `occupation-name=${searchAdsQuery.occupationName}&`
    }
    if (searchAdsQuery.occupationGroup) {
      url += `occupation-group=${searchAdsQuery.occupationGroup}&`
    }
    if (searchAdsQuery.occupationField) {
      url += `occupation-field=${searchAdsQuery.occupationField}&`
    }
    if (searchAdsQuery.occupationCollection) {
      url += `occupation-collection=${searchAdsQuery.occupationCollection}&`
    }
    if (searchAdsQuery.skill) {
      url += `skill=${searchAdsQuery.skill}&`
    }
    if (searchAdsQuery.language) {
      url += `language=${searchAdsQuery.language}&`
    }
    if (searchAdsQuery.worktimeExtent) {
      url += `worktime-extent=${searchAdsQuery.worktimeExtent}&`
    }
    if (searchAdsQuery.parttimeMin) {
      url += `parttime.min=${searchAdsQuery.parttimeMin}&`
    }
    if (searchAdsQuery.parttimeMax) {
      url += `parttime.max=${searchAdsQuery.parttimeMax}&`
    }
    if (searchAdsQuery.drivingLicenseRequired) {
      url += `driving-license-required=${searchAdsQuery.drivingLicenseRequired}&`
    }
    if (searchAdsQuery.drivingLicense) {
      url += `driving-license=${searchAdsQuery.drivingLicense}&`
    }
    if (searchAdsQuery.employmentType) {
      url += `employment-type=${searchAdsQuery.employmentType}&`
    }
    if (searchAdsQuery.experience) {
      url += `experience=${searchAdsQuery.experience}&`
    }
    if (searchAdsQuery.municipality) {
      url += `municipality=${searchAdsQuery.municipality}&`
    }
    if (searchAdsQuery.region) {
      url += `region=${searchAdsQuery.region}&`
    }
    if (searchAdsQuery.country) {
      url += `country=${searchAdsQuery.country}&`
    }
    if (searchAdsQuery.unspecifiedSwedenWorkplace) {
      url += `unspecified-sweden-workplace=${searchAdsQuery.unspecifiedSwedenWorkplace}&`
    }
    if (searchAdsQuery.abroad) {
      url += `abroad=${searchAdsQuery.abroad}&`
    }
    if (searchAdsQuery.remote) {
      url += `remote=${searchAdsQuery.remote}&`
    }
    if (searchAdsQuery.openForAll) {
      url += `open_for_all=${searchAdsQuery.openForAll}&`
    }
    if (searchAdsQuery.trainee) {
      url += `trainee=${searchAdsQuery.trainee}&`
    }
    if (searchAdsQuery.larling) {
      url += `larling=${searchAdsQuery.larling}&`
    }
    if (searchAdsQuery.franchise) {
      url += `franchise=${searchAdsQuery.franchise}&`
    }
    if (searchAdsQuery.hireWorkPlace) {
      url += `hire-work-place=${searchAdsQuery.hireWorkPlace}&`
    }
    if (searchAdsQuery.position) {
      url += `position=${searchAdsQuery.position}&`
    }
    if (searchAdsQuery.positionRadius) {
      url += `position.radius=${searchAdsQuery.positionRadius}&`
    }
    if (searchAdsQuery.employer) {
      url += `employer=${searchAdsQuery.employer}&`
    }
    if (searchAdsQuery.q) {
      url += `q=${searchAdsQuery.q}&`
    }
    if (searchAdsQuery.qfields) {
      url += `qfields=${searchAdsQuery.qfields}&`
    }
    if (searchAdsQuery.relevanceThreshold) {
      url += `relevance-threshold=${searchAdsQuery.relevanceThreshold}&`
    }
    if (searchAdsQuery.resdet) {
      url += `resdet=${searchAdsQuery.resdet}&`
    }
    if (searchAdsQuery.offset) {
      url += `offset=${searchAdsQuery.offset}&`
    }
    if (searchAdsQuery.limit) {
      url += `limit=${searchAdsQuery.limit}&`
    }
    if (searchAdsQuery.sort) {
      url += `sort=${searchAdsQuery.sort}&`
    }
    if (searchAdsQuery.stats) {
      url += `stats=${searchAdsQuery.stats}&`
    }
    if (searchAdsQuery.statsLimit) {
      url += `stats.limit=${searchAdsQuery.statsLimit}&`
    }
    if (searchAdsQuery.requestTimeout) {
      url += `request-timeout=${searchAdsQuery.requestTimeout}&`
    }
    return url;
  }

  private createSearchHistoricalAdsURI(searchAdsQuery: SearchAdsQuery): string {

    let baseUrl = `${this.historicalAdsUrl}/search?`;
    let url = this.createBaseSearchAdsURI(baseUrl, searchAdsQuery);

    if (searchAdsQuery.historicalFrom) {
      const historicalFrom = this.datepipe.transform(searchAdsQuery.historicalFrom, 'yyyy-MM-dd');
      url += `historical-from=${historicalFrom}&`
    }
    if (searchAdsQuery.historicalTo) {
      const historicalTo = this.datepipe.transform(searchAdsQuery.historicalTo, 'yyyy-MM-dd');
      url += `historical-to=${historicalTo}&`
    }
    if (searchAdsQuery.startSeasonalTime) {
      const startSeasonalTime = this.datepipe.transform(searchAdsQuery.startSeasonalTime, 'yyyy-MM-dd');
      url += `start-seasonal-time=${startSeasonalTime}&`
    }
    if (searchAdsQuery.endSeasonalTime) {
      const endSeasonalTime = this.datepipe.transform(searchAdsQuery.endSeasonalTime, 'yyyy-MM-dd');
      url += `end-seasonal-time=${endSeasonalTime}&`
    }

    if (url.endsWith("&")) {
      url = url.slice(0, -1);
    }
    return url
  }


  // 'https://education-api-ed-api-develop.test.services.jtech.se/educations?query=system&education_type=program&education_form=h%C3%B6gskoleutbildning
  public searchAds(searchAdsQuery: SearchAdsQuery | undefined,
    pageNumber: number | undefined, pageSize: number | undefined): void {
    this.isLoadingPageResult = true;
    let searchType = 'historical'
    this._searchAdsQuery = searchAdsQuery;
    let url = '';
    if (!searchAdsQuery) {
      console.warn(`Historical query does not contain values`);
      return;
    }
    if (searchAdsQuery) {
      console.warn(`Historical query contain values`);
    }
    if (searchAdsQuery) {
      url = this.createSearchHistoricalAdsURI(searchAdsQuery);
    }

    // https://historical.api.jobtechdev.se</search?q=frontendutvecklare&offset=0&limit=10&request-timeout=300>'
    this.httpClient.get(url)
      .pipe(catchError(this.handleError)).subscribe(data => {

        this._searchedAds = this.handleSearchedAdsResponse(data, pageNumber, pageSize, searchType);
        this.isLoadingPageResult = false;
      });

  }

  handleError = (error: HttpErrorResponse) => {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(`Backend returned code ${error.status}, body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    this.isLoadingPageResult = false;
    return throwError(
      'Something bad happened; please try again later.');
  }

  public handleSearchedAdsResponse(searchedAdsIn: any, pageNumber: number | undefined, pageSize: number | undefined, searchType: string | undefined): SearchedAds {
    const searchedAdsOut = new SearchedAds();
    if (searchedAdsIn && searchedAdsIn['total']) {
      searchedAdsOut.totalHits = searchedAdsIn['total']['value'];
      searchedAdsOut.pageNumber = pageNumber;
      searchedAdsOut.pageSize = pageSize;
    }

    if (searchedAdsIn && searchedAdsIn['positions']) {
      searchedAdsOut.totalPositions = searchedAdsIn['positions'];
    }

    if (searchedAdsIn && searchedAdsIn['hits']) {
      for (let adIn of searchedAdsIn['hits']) {
        let searchedAd: SearchedAd = this.adMapperService.adInToSearchedAdMapper(adIn, searchType);
        searchedAdsOut.ads.push(searchedAd);
      }
    }
    return searchedAdsOut;
  }

}
