import {MatPaginatorIntl} from '@angular/material/paginator';
import {Subject} from 'rxjs';
import {Injectable} from "@angular/core";

@Injectable()
export class PaginatorImpl implements MatPaginatorIntl {
  changes = new Subject<void>();

  firstPageLabel = "Första sidan";
  itemsPerPageLabel = "Antal annonser per sida";
  lastPageLabel = "Sista sidan";

  nextPageLabel = "Nästa sida";
  previousPageLabel = "Föregående sida";

  getRangeLabel(page: number, pageSize: number, length: number): string {
    if (length === 0) {
      return "Sida 1 av 1";
    }
    const amountPages = Math.ceil(length / pageSize);
    return `Sida ${page + 1} av ${amountPages}`;
  }
}
