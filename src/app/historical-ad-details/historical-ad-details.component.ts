import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { FetchHistoricalAdService } from "../services/fetch-historical-ad.service";
import { SearchedAd } from "../model/searched-ad.model";
import { faLocationDot } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-historical-ad-details',
  templateUrl: './historical-ad-details.component.html',
  styleUrls: ['./historical-ad-details.component.scss']
})
export class HistoricalAdDetailsComponent implements OnInit {
  // FontAwesome icon
  faLocationDot = faLocationDot;

  // Properties for ad details
  id: string | null = '';
  ad: SearchedAd | undefined;

  constructor(
    private route: ActivatedRoute,
    private fetchHistoricalAdService: FetchHistoricalAdService
  ) { }

  ngOnInit(): void {
    this.loadAdDetails();
  }

  // Fetch ad details based on the route parameter
  loadAdDetails(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.id = id;
      this.fetchHistoricalAdService.fetchHistoricalAd(id)
        .subscribe(ad => this.ad = ad);
    }
  }

  // Helper method to get readable labels for ad requirements
  getRequirementLabel(requirement: string | undefined): string {
    if (!requirement) return '';
    switch (requirement) {
      case 'workExperiences':
        return 'Arbetslivserfarenhet';
      case 'education':
        return 'Utbildning';
      case 'skills':
        return 'Kompetenser';
      case 'languages':
        return 'Språk';
      default:
        return '';
    }
  }

  // Helper method to get readable labels for the source type
  getSourceLabel(sourceType: string | undefined): string {
    if (!sourceType) return '';
    switch (sourceType) {
      case 'VIA_ANNONSERA':
        return 'Arbetsförmedlingens annonseringsverktyg';
      case 'VIA_PLATSBANKEN_DXA':
        return 'Direktöverförda (DXA)';
      default:
        return sourceType;
    }
  }
}
