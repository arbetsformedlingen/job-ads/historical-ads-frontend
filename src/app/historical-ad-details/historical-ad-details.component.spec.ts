import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalAdDetailsComponent } from './historical-ad-details.component';

describe('HistoricalAdDetailsComponent', () => {
  let component: HistoricalAdDetailsComponent;
  let fixture: ComponentFixture<HistoricalAdDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoricalAdDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HistoricalAdDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
