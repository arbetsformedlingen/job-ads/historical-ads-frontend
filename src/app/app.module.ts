import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HistoricalAdsSearchComponent } from './historical-ads-search/historical-ads-search.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatInputModule } from "@angular/material/input";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { FlexLayoutModule } from '@ngbracket/ngx-layout';
import { MatIconModule } from "@angular/material/icon";
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldModule } from "@angular/material/form-field";
import { RouterModule } from "@angular/router";
import { MatButtonModule } from "@angular/material/button";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatCardModule } from "@angular/material/card";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatSelectModule } from "@angular/material/select";
import { MatTabsModule } from "@angular/material/tabs";
import { MatSliderModule } from "@angular/material/slider";
import { MatDividerModule } from "@angular/material/divider";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MAT_DATE_LOCALE, MatNativeDateModule } from "@angular/material/core";
import { HistoricalAdDetailsComponent } from './historical-ad-details/historical-ad-details.component';
import { CommonModule, DatePipe } from "@angular/common";
import { LinkHttpPipe } from "./pipes/link-http.pipe";
import { MatPaginatorIntl, MatPaginatorModule } from "@angular/material/paginator";
import { PaginatorImpl } from "./paginator";
import { CustomSlice } from "./pipes/custom-slice.pipe";
import { MatRadioModule } from "@angular/material/radio";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatProgressBarModule } from "@angular/material/progress-bar";

@NgModule({
  declarations: [
    AppComponent,
    HistoricalAdsSearchComponent,
    HistoricalAdDetailsComponent,
    LinkHttpPipe,
    CustomSlice
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlexLayoutModule,
    MatIconModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatButtonToggleModule,
    FontAwesomeModule,
    MatProgressSpinnerModule,
    AppRoutingModule,
    MatAutocompleteModule,
    MatCardModule,
    MatExpansionModule,
    MatSelectModule,
    MatTabsModule,
    MatDividerModule,
    MatSliderModule,
    MatDatepickerModule,
    MatNativeDateModule,
    CommonModule,
    MatPaginatorModule,
    MatRadioModule,
    MatTooltipModule,
    MatProgressBarModule
  ],
  providers: [{ provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' }, }, DatePipe,
  { provide: MatPaginatorIntl, useClass: PaginatorImpl }, { provide: MAT_DATE_LOCALE, useValue: 'sv' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
