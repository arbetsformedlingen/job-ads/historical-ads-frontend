# HistoricalAdsFrontend

A reference implementation for historical ads implemented in Angular.

## Install local development environment
1. Check package.json for current version of Angular used in the app (as of writing it is v 17.x).
2. Check current version of nodeJS (if installed) and that it's compatible with the Angular version: `node -v` 
3. Follow   
   https://angular.io/guide/setup-local#prerequisites
   https://angular.io/guide/setup-local#install-the-angular-cli
   ...to install the correct versions of nodeJS and the Angular CLI if not already done.
4. cd to root folder, historical-ads-frontend
   Run command: npm install
5. To start local server, run command: ng serve
   The server will run at `http://localhost:4200/`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Handle environment variables
The common solution to handle env.variables in Angular is to provide env.files in environments folder, that matches the target environment.
One big downfall is lack of support for build once install anywhere pattern, because the env.variables are bundled into the build artifact.
Therefore in this app a new env.file (i.e. environment.json) is provided in assets folder and the file is not bundled in build time.
So if the file exist it is used, otherwise the common default values in environments folder are used. This works the same when
running ng serve as well as when running inside a (docker) container.

See historical-ads-frontend-infra repo for how to overwrite values in environment.json when installing on Open Shift using Jobtech ci/cd.

This solution is based on the ideas described on the page:
https://medium.com/voobans-tech-stories/multiple-environments-with-angular-and-docker-2512e342ab5a

### Increment version number
To avoid dependency to education-frontend-app-infra the version nr field is set in src/environments/version.ts

## Run in docker
Run `docker build -t historical-ads-frontend:v1.0.0 -f ./Dockerfile .` in root dir to build (i.e set preferred version number).
Run `docker run -p 8080:8080 -d historical-ads-frontend:v1.0.0` to start app

To change environment settings, copy environment file (e.g. prod-env.json) to /assets/environments/environment.json (i.e. replace environment.json),
see Handle environment variables section above.

## Upgrade Angular versions
New versions of Angular are frequently released, so here is some hints how to upgrade:
- Upgrade in a new git feature branch so that you can delete it and start all over if the upgrade fails.
- In general, follow the guide: https://update.angular.io/
  Don't forget to check node version and to install the angular version to upgrade to, e.g. `npm i @angular/cli@[version]`
- Upgrade to next Angular version instead of jumping up several versions.
  To upgrade multiple versions at once will only cause upgrade problems.
- After following the general guide you should upgrade all libs in package.json that weren't covered in the guide.
  Usually the major version number of the lib is compatible with the corresponding Angular version. In some cases there
  is a map that shows version compatibility.
- Commit changes to source repository (i.e. git) after every command that affects the config, e.g. package.json and package-lock.json.
- To avoid having to solve all dependency of dependency problems, install libs with the flag --force. But make sure
  there is compatibility between the libs at the end of the upgrade.
- Make sure the node version in Dockerfile is compatible with the new Angular version.

### Update Angular to latest minor version
Run:
`npm install`
`ng update @angular/cli@^17 @angular/core@^17 @angular/cdk@^17 @angular/material@^17`

In case of errors when updating material or cdk, search for the latest minor version and set exact version to install:  
`ng update @angular/material@17.3.10`
`ng update @angular/cdk@17.3.10`

### List installed packages and versions
npm list --depth=0


## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

